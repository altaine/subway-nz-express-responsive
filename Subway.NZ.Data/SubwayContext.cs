﻿using Subway.Data.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subway.NZ.Data
{
    public class SubwayContext : DbContext
    {
        public virtual IDbSet<MenuOption> MenuOptions { get; set; }
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<Store> Stores { get; set; }
        public virtual IDbSet<StoreHours> StoreHours { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
