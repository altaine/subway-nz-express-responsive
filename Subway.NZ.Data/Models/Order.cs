﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Subway.NZ.Data.Models
{
    public class Order
    {
        public Order()
        {
            this.Items = new List<Cart.CartItem>();
            this.SelectedStore = new Store();
            this.OrderMode = "iPhone";
        }

        public string OrderMode { get; set; }
        public DateTime Date { get; set; }
        public List<Cart.CartItem> Items { get; set; }
        public string Name { get; set; }
        public int OrderId { get; set; }
        public DateTime Pickup { get; set; }
        public Store SelectedStore { get; set; }

        public decimal Total
        {
            get
            {
                return Items.Sum(x => x.Quantity * x.Price);
            }
        }
    }
}
