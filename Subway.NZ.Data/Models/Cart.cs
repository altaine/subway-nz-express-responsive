﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subway.NZ.Data.Models
{
    public class Cart
    {
        public Cart()
        {
            this.Items = new List<CartItem>();
            this.SelectedStore = new Store();
        }

        public List<CartItem> Items { get; set; }
        public Store SelectedStore { get; set; }

        public decimal Total
        {
            get
            {
                return Items.Sum(x => x.Quantity * x.Price);
            }
        }

        public class CartItem
        {
            public string Code{ get; set; }
            public string ProductName { get; set; }
            public decimal Price { get; set; }
            public int Quantity { get; set; }
            public string ImageUrl { get; set; }
        }

        public class Store
        {
            public int StoreId { get; set; }
            public string StoreName { get; set; }
        }
    }
}
