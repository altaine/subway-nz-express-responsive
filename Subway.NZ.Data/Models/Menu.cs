﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subway.NZ.Data.Models
{
    public class Menu
    {
        public List<Category> Categories { get; set; }

        public class Category
        {
            public Category()
            {
                this.SubCategories = new Category[] { };
            }

            public string Name { get; set; }
            public string Title { get; set; }
            public List<Product> Products { get; set; }
            public bool ShowMakeItA_Box { get; set; }
            public string Layout { get; set; }
            public Category[] SubCategories { get; set; }
            public string Image { get; internal set; }
        }

        public class Product
        {
            public int MenuOptionId { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string ImageUrl { get; set; }
            public bool IsHotSub { get; internal set; }
            public decimal Price { get; set; }
            public string Serves { get; internal set; }
            public string ServesPortions { get; internal set; }
        }        
    }
}
