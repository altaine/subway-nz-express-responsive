﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subway.NZ.Data.Models
{
    public class Store
    {
        public int SiteId { get; set; }
        public string Name { get; set; }
        public bool AcceptsCreditCards { get; set; }
    }
}
