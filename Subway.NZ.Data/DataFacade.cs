﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Subway.Data.EntityFramework;
using System.Configuration;
using Subway.NZ.Data.Models;

namespace Subway.NZ.Data
{
    public class DataFacade
    {
        private SubwayContext context = new SubwayContext();
        public async Task<Models.Menu> GetMenuAsync()
        {
            var all = await context.MenuOptions.Where(x => x.IsActive).ToListAsync();

            var model = new Models.Menu { Categories = new List<Models.Menu.Category>() };

            // NOTE: this is just a guess. To refine
            var topLevel = all.Where(x => x.IsActive).ToList();

            model.Categories.Add(new Models.Menu.Category
            {
                Name = "Classic", Title = "CHOOSE YOUR SUB", Layout = "Category_Sub",
                ShowMakeItA_Box = true,
                Products = topLevel.Where(x => string.Equals(x.MenuOptionTypeCode,"MNU/START", StringComparison.InvariantCultureIgnoreCase) && 
                    string.Equals(x.Category, "SUB", StringComparison.InvariantCultureIgnoreCase) && x.MenuOptionDisplayCategoryID == 1 /*Standard*/)
                    .Select(MapProduct).ToList()
            });

            model.Categories.Add(new Models.Menu.Category
            {
                Name = "Lites", Title = "CHOOSE YOUR SUB",
                Layout = "Category_Sub",
                ShowMakeItA_Box = true,
                Products = topLevel.Where(x => string.Equals(x.MenuOptionTypeCode, "MNU/START", StringComparison.InvariantCultureIgnoreCase) && 
                string.Equals(x.Category, "SUB", StringComparison.InvariantCultureIgnoreCase) && x.MenuOptionDisplayCategoryID == 2 /*Lite*/)
                .Select(MapProduct).ToList()
            });

            model.Categories.Add(new Models.Menu.Category
            {
                Name = "Special",
                  Layout = "Category_Sub",
                Products = topLevel.Where(x => string.Equals(x.MenuOptionTypeCode, "MNU/START", StringComparison.InvariantCultureIgnoreCase) &&
                    string.Equals(x.Category, "SUB", StringComparison.InvariantCultureIgnoreCase) && x.MenuOptionDisplayCategoryID == 1)
                .Take(2) // takeing the first two as a sample
                .Select(MapProduct).ToList()
            });


            model.Categories.Add(new Models.Menu.Category
            {
                Name = "Breakfast",
                Layout = "",
                Title = "CHOOSE YOUR BREAKFAST",
                Products = topLevel.Where(x => string.Equals(x.Category, "Breakfast", StringComparison.InvariantCultureIgnoreCase)).Select(MapProduct).ToList()
            });


            model.Categories.Add(new Models.Menu.Category
            {
                Name = "Kids Pak™",
                Layout = "",
                Title = "Kids Pak™",
                Products = topLevel.Where(x => string.Equals(x.MenuOptionTypeCode, "MNU/LENGTH", StringComparison.InvariantCultureIgnoreCase) && 
                string.Equals(x.Category, "KidsPak", StringComparison.InvariantCultureIgnoreCase)).Select(MapProduct).ToList()
            });

            var cateringSubCategories = new Models.Menu.Category[]
            {
                new Menu.Category
                {
                    Image = "/Images/Catering/subway-classic.jpg",
                    Name="Subway® classic", Title = "VEGGIE DELITE®, Chicken Strips, SUBWAY CLUB®, Italian B.M.T.®, Tuna",
                    Products = new List<Menu.Product>
                    {
                        new Menu.Product { Name = "Sandwich platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"},
                        new Menu.Product { Name = "Wrap platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"}
                    }
                },
                new Menu.Category
                {
                    Image = "/Images/Catering/veggie-feast.jpg",
                    Name="Veggie Feast", Title = "VEGGIE DELITE®",
                    Products = new List<Menu.Product>
                    {
                        new Menu.Product { Name = "Sandwich platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"},
                        new Menu.Product { Name = "Wrap platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"}
                    }
                }
                ,
                new Menu.Category
                {
                    Image = "/Images/Catering/meat-feast.jpg",
                    Name="Meat feast", Title = "Italian B.M.T.®, SUBWAY CLUB®, Turkey & Ham, Chicken Strips, Roast Beef",
                    Products = new List<Menu.Product>
                    {
                        new Menu.Product { Name = "Sandwich platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"},
                        new Menu.Product { Name = "Wrap platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"}
                    }
                },
                new Menu.Category
                {
                    Image = "/Images/Catering/subway-lites.jpg",
                    Name="Subway® lites", Title = "VEGGIE DELITE®, Roast Beef, Chicken Strips, Turkey, Ham",
                    Products = new List<Menu.Product>
                    {
                        new Menu.Product { Name = "Sandwich platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"},
                        new Menu.Product { Name = "Wrap platter", Price = 54, Serves = "Serves 5-9", ServesPortions = "(15 portions)"}
                    }
                },
                new Menu.Category
                {
                    Image = "/Images/Catering/cookies.jpg",
                    Name="Cookies", Title = "",
                    Products = new List<Menu.Product>
                    {
                        new Menu.Product { Name = "Sandwich platter", Price = 7.80m, Serves = "12 cookies", ServesPortions = ""},
                        new Menu.Product { Name = "Wrap platter", Price = 29, Serves = "36 cookies", ServesPortions = ""}
                    }
                }
            };

            model.Categories.Add(new Models.Menu.Category
            {
                Name = "Catering",
                Layout = "Category_Catering",
                Products = new List<Menu.Product>(), // topLevel.Where(x => string.Equals(x.MenuOptionTypeCode, "MNU/CATER", StringComparison.InvariantCultureIgnoreCase) &&string.Equals(x.Category, "Catering", StringComparison.InvariantCultureIgnoreCase)).Select(MapProduct).ToList()
                SubCategories = cateringSubCategories,                
            });




            return model;
        }

        public async Task<List<Menu.Product>> GetLengthsAsync()
        {
            var all = await context.MenuOptions.Where(x => x.IsActive && x.MenuOptionTypeCode == "MNU/LENGTH" && (x.Category == "Sub" || x.Category == "Salad" || x.Category == "Wrap")).ToListAsync();
            var mapped = all.Select(MapProduct).ToList();
            return mapped;
        }

        public async Task<List<Menu.Product>> GetSidesAsync()
        {
            var all = await context.MenuOptions.Where(x => x.IsActive && x.MenuOptionTypeCode == "MNU/VALUE").ToListAsync();
            var mapped = all.Select(MapProduct).ToList();
            return mapped;
        }

        public async Task<List<Menu.Product>> GetDrinksAsync()
        {
            var all = await context.MenuOptions.Where(x => x.IsActive && x.MenuOptionTypeCode == "MNU/SIDES" && x.SortOrder == 4).ToListAsync();
            var mapped = all.Select(MapProduct).ToList();
            return mapped;
        }

        public async Task<List<Menu.Product>> GetAdditionalItemsAsync()
        {
            var all = await context.MenuOptions.Where(x => x.IsActive && x.MenuOptionTypeCode == "MNU/SIDES" && (x.SortOrder == 4 || x.SortOrder == 8 || x.SortOrder == 10)).ToListAsync();
            var mapped = all.Select(MapProduct).ToList();
            return mapped;
        }

        private Models.Menu.Product MapProduct(MenuOption m)
        {
            var r = new Random();
            return new Models.Menu.Product
            {
                MenuOptionId = m.MenuOptionId,
                Code = m.Code,
                Name = m.Name,
                ImageUrl = $"{ConfigurationManager.AppSettings["ProductImagesBaseUrl"]}{ m.WebImageUrl }",
                IsHotSub = m.IsHotSub.GetValueOrDefault(),
                Price = (r.Next(50,999))*0.01m
            };
        }
    }
}

