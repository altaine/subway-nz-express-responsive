﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Subway.Data.EntityFramework
{
    public class Store
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StoreID { get; set; }

        [MaxLength(10)]
        public string Code { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Location { get; set; }

        [MaxLength(255)]
        public string Address { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }

        [MaxLength(255)]
        public string PrintingTypeCode { get; set; }

        public string StoreHours { get; set; }

        [MaxLength(50)]
        public string MobileNumber { get; set; }

        [MaxLength(50)]
        public string FaxNumber { get; set; }

        [MaxLength(255)]
        public string EmailAddress { get; set; }

        public bool SendAlerts { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public int RegionID { get; set; }

        public int PreorderPrintTime { get; set; }

        public int OrderLeadTime { get; set; }

        public Guid? UserID { get; set; }

        [MaxLength(10)]
        public string StoreNumber { get; set; }

        public int BreakfastLeadTime { get; set; }

        [MaxLength(10)]
        public string TTLFriendlyStoreNumber { get; set; }

        public bool OffersDelivery { get; set; }

        public bool IsHalal { get; set; }

        [MaxLength(50)]
        public string ContactName { get; set; }
        [MaxLength(50)]
        public string ContactPhoneNumber { get; set; }
        [MaxLength(1000)]
        public string AlertsMobileNumber { get; set; }
        [MaxLength(250)]
        public string MapFilename { get; set; }

        [MaxLength(20)]
        public string Easting { get; set; }

        [MaxLength(20)]
        public string Northing { get; set; }

        [MaxLength(20)]
        public string Latitude { get; set; }

        [MaxLength(20)]
        public string Longitude { get; set; }

        public decimal? DeliveryRadiusKilometers { get; set; }

        [MaxLength(255)]
        public string ContactEmailAddress { get; set; }
        [MaxLength(100)]
        public string SecondaryContactName { get; set; }
        [MaxLength(50)]
        public string SecondaryContactPhoneNumber { get; set; }


        [MaxLength(255)]
        public string SecondaryContactEmailAddress { get; set; }

        [MaxLength(100)]
        public string FranchiseeName { get; set; }

        [MaxLength(100)]
        public string DATerritoryName { get; set; }

        [MaxLength(100)]
        public string IFAFMarketName { get; set; }

        [MaxLength(15)]
        public string VATNumber { get; set; }

        [MaxLength(100)]
        public string RegisteredCompanyName { get; set; }

        [MaxLength(100)]
        public string StoreManagerName { get; set; }

        [MaxLength(100)]
        public string StoreManagerMobileNumber { get; set; }

        [MaxLength(20)]
        public string PosSystem { get; set; }

        [MaxLength(20)]
        public string PosSupport { get; set; }

        [MaxLength(50)]
        public string RegisteredCompanyNumber { get; set; }

        public bool OffersCODOrders { get; set; }

        public bool OffersHouseAccountOrders { get; set; }

        public bool OffersCreditCardOrders { get; set; }

        public int? PaymentProviderID { get; set; }

        [MaxLength(300)]
        public string PaymentProviderSalt { get; set; }

        [MaxLength(300)]
        public string PaymentProviderPassword { get; set; }

        [MaxLength(100)]
        public string PaymentProviderMerchantID { get; set; }

        public int? SalesRegionID { get; set; }

        public bool UseCreditCardPageFunnel { get; set; }

        public bool? IgnorePromotionPrice { get; set; }

        [MaxLength(100)]
        public string TradingAsName { get; set; }

        [MaxLength(500)]
        public string InvoiceAddress { get; set; }

        public bool OffersCollectionOnWeekend { get; set; }
        
    }
}