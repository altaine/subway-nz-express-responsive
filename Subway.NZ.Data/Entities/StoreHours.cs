﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Subway.Data.EntityFramework
{
    public class StoreHours
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StoreHoursID { get; set; }
        public int StoreID { get; set; }
        public int DayOfWeek { get; set; }
        [MaxLength(20)]
        public  string TimeOpen { get; set; }
        [MaxLength(20)]
        public string TimeClosed { get; set; }
        public int OrderSystemID { get; set; }

        [NotMapped]
        public int OpenHour => GetStoreHourPart(StoreHourPart.OpenHour);

        [NotMapped]
        public int OpenMinute => GetStoreHourPart(StoreHourPart.OpenMinute);

        [NotMapped]
        public int CloseHour => GetStoreHourPart(StoreHourPart.CloseHour);

        [NotMapped]
        public int CloseMinute => GetStoreHourPart(StoreHourPart.CloseMinute);

        private enum StoreHourPart
        {
            OpenHour,
            OpenMinute,
            CloseHour,
            CloseMinute
        }

        private int GetStoreHourPart(StoreHourPart part)
        {
            int index = 0;
            if (part == StoreHourPart.OpenMinute || part == StoreHourPart.CloseMinute)
                index = 1;

            string[] hourMinute;
            if (part == StoreHourPart.OpenHour || part == StoreHourPart.OpenMinute)
                hourMinute = TimeOpen.Split(':');
            else
                hourMinute = TimeClosed.Split(':');

            return Convert.ToInt32(hourMinute[index]);
        }
    }
}