﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Subway.Data.EntityFramework
{
    public class Order 
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderID { get; set; }
        public int StoreID { get; set; }
        [MaxLength(50)]
        public string MobileOriginate { get; set; }
        public string Keyword { get; set; }
        public int PaymentStatusId { get; set; }
        public int? BarclaycardTransactionStatusID { get; set; }
        public decimal TotalCost { get; set; }
        public bool SentToPrinter { get; set; }
        public DateTime? DateRequired { get; set; }
        public DateTime? DatePrinted { get; set; }
        public DateTime? RequiredPrintTime { get; set; }
        public string OrderItemText { get; set; }
    }
}