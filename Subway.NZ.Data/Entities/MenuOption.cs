﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subway.Data.EntityFramework
{
    public class MenuOption
    {
        public int MenuOptionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string PrintName { get; set; }
        public string MenuOptionTypeCode { get; set; }
        public string Category { get; set; }
        public short? SortOrder { get; set; }
        public bool IncludeInOnline { get; set; }
        public short? WebSortOrder { get; set; }
        public bool IsActive { get; set; }
        /*public string SmsCode { get; set; }
        public string MenuOptionCategory { get; set; }
        public int? OnlineSortOrder { get; set; }*/
        public bool? IsHotSub { get; set; }
        public int? MenuOptionDisplayCategoryID { get; set; }
        public int? ItemCount { get; set; }
        public string ImageUrl { get; set; }
        /// <summary>
        /// Display name for mobile devices.
        /// </summary>
        public string MobileName { get; set; }

        /// <summary>
        /// Display extra info about the menu option.
        /// </summary>
        public string ExtraInfo { get; set; }

        /// <summary>
        /// The menu options display identifier for Non-Halal and Halal stores
        /// </summary>
        public int MenuOptionStoreDisplayID { get; set; }

        /// <summary>
        /// The order system the menu option belongs to
        /// </summary>
        public int OrderSystemID { get; set; }

        public int MenuOptionClassificationID { get; set; }

        /// <summary>
        /// Get or set the web image URL
        /// </summary>
        public string WebImageUrl { get; set; }

        /// <summary>
        /// Get or set the mobile retina image URL 
        /// This is for iPhone only
        /// </summary>
        public string MobileRetinaImageUrl { get; set; }

    }
}
