﻿using Subway.NZ.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Subway.NZ.Data;
using System.Threading.Tasks;

namespace Subway.NZ.Web.Controllers
{
    public class ExpressOrderingController : Controller
    {
        private readonly DataFacade DataFacade;

        public ExpressOrderingController()
        {
            // to replace with DI
            this.DataFacade = new Data.DataFacade();
        }

        public async Task<ActionResult> Index()
        {
            OrderingModel model = await GetModel();
            return View(model);
        }

        private async Task<OrderingModel> GetModel()
        {
            var model = new OrderingModel
            {
                Menu = await DataFacade.GetMenuAsync(),
                Lengths = await DataFacade.GetLengthsAsync(),
                Drinks = await DataFacade.GetDrinksAsync(),
                Sides = await DataFacade.GetSidesAsync(),
                AdditionalItems = await DataFacade.GetAdditionalItemsAsync(),
                Balance = new OrderingModel.CustomerBalance
                {
                    CardNumber = "1234567890123456",
                    RewardsBalance = 3.28m,
                    GiftsBalance = 30m
                }
            };

            var p1 = model.Menu.Categories.First().Products.ElementAt(0);
            var p2 = model.Menu.Categories.First().Products.ElementAt(1);

            model.Cart = new Data.Models.Cart()
            {
                SelectedStore = new Data.Models.Cart.Store { StoreId = 1, StoreName = "Some store somewhere" },
                Items = new List<Data.Models.Cart.CartItem>
                 {
                    new Data.Models.Cart.CartItem { Code = p1.Code, ProductName = p1.Name, Quantity = 1, Price = p1.Price, ImageUrl = p1.ImageUrl },
                    new Data.Models.Cart.CartItem { Code = p2.Code, ProductName = p2.Name, Quantity = 2, Price = p2.Price, ImageUrl = p2.ImageUrl }
                 },                
            };

            model.Stores = new List<Data.Models.Store>
            {
                new Data.Models.Store { SiteId = 1, Name = "London store", AcceptsCreditCards = true},
                new Data.Models.Store { SiteId = 2, Name = "Bristol store", AcceptsCreditCards = false},
            };


            model.RecentOrders = new List<Data.Models.Order>
            {
                new Data.Models.Order { OrderId = 12345, Date = DateTime.Now, Pickup = DateTime.Now.AddMinutes(30), Name = "Order 1",
                Items = new List<Data.Models.Cart.CartItem>
                 {
                    new Data.Models.Cart.CartItem { Code = p1.Code, ProductName = p1.Name, Quantity = 1, Price = p1.Price, ImageUrl = p1.ImageUrl },
                    new Data.Models.Cart.CartItem { Code = p2.Code, ProductName = p2.Name, Quantity = 2, Price = p2.Price, ImageUrl = p2.ImageUrl }
                 }},
                new Data.Models.Order { OrderId = 12346, Date = DateTime.Now, Pickup = DateTime.Now.AddMinutes(30), Name = "Order 2",
                Items = new List<Data.Models.Cart.CartItem>
                 {
                    new Data.Models.Cart.CartItem { Code = p1.Code, ProductName = p1.Name, Quantity = 1, Price = p1.Price, ImageUrl = p1.ImageUrl },
                    new Data.Models.Cart.CartItem { Code = p2.Code, ProductName = p2.Name, Quantity = 2, Price = p2.Price, ImageUrl = p2.ImageUrl }
                 }}
            };

            return model;
        }

        public async Task<ActionResult> Step2()
        {
            OrderingModel model = await GetModel();
            return View(model);
        }
        public async Task<ActionResult> Step3()
        {
            OrderingModel model = await GetModel();
            return View(model);
        }
        public async Task<ActionResult> Step4()
        {
            OrderingModel model = await GetModel();
            return View(model);
        }
        public async Task<ActionResult> Step5()
        {
            OrderingModel model = await GetModel();
            return View(model);
        }

        public async Task<ActionResult> School()
        {
            OrderingModel model = await GetModel();
            return View(model);
        }

        public async Task<ActionResult> MyLastOrder()
        {
            OrderingModel model = await GetModel();
            return View(model);
        }
    }
}