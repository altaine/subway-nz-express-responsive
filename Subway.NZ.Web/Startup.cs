﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Subway.NZ.Web.Startup))]
namespace Subway.NZ.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
