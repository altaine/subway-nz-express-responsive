﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subway.NZ.Web.Models
{
    public class ModalMessage
    {
        public string IconClass { get; set; }
        public string ControlId { get; set; }
        public string Message { get; set; }
        public string DismissButtonText { get; set; }
    }
}