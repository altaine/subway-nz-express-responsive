﻿using Subway.NZ.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subway.NZ.Web.Models
{
    public class OrderingModel
    {
        public Menu Menu { get; set; }
        public List<Menu.Product> Lengths { get; set; }
        public List<Menu.Product> Drinks { get; set; }
        public List<Menu.Product> Sides { get; set; }
        public List<Menu.Product> AdditionalItems { get; set; }

        public Cart Cart { get; set; }
        public List<Store> Stores { get; set; }
        public List<Order> RecentOrders { get; set; }

        public CustomerBalance Balance { get; set; }

        public class CustomerBalance
        {
            public string CardNumber { get; set; }

            public decimal RewardsBalance { get; set; }
            public decimal GiftsBalance { get; set; }
            public decimal Balance { get { return RewardsBalance + GiftsBalance; } }
        }
    }
}